resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id
  ]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance"
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port = 1521
    to_port   = 1521
    protocol  = "tcp"
    security_groups = [
      aws_security_group.lb.id,
      aws_security_group.bastion.id
    ]
  }

  tags = local.common_tags
}

resource "aws_db_instance" "main" {
  identifier              = "${local.prefix}-db"
  name                    = "tnlmsvc"
  allocated_storage       = 20
  engine                  = "oracle-ee"
  engine_version          = "12.2.0.1.ru-2020-07.rur-2020-07.r1"
  instance_class          = "db.m5.large"
  storage_type            = "gp2"
  db_subnet_group_name    = aws_db_subnet_group.main.name
  password                = var.db_password
  username                = var.db_username
  backup_retention_period = 0
  publicly_accessible     = false
  deletion_protection     = false
  multi_az                = false
  skip_final_snapshot     = true
  character_set_name      = "AL32UTF8"
  vpc_security_group_ids  = [aws_security_group.rds.id]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}
